<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Duc Hiep - @yield('title')</title>
	<link rel="stylesheet" href="">
	<style type="text/css" media="screen">
		#wrapper{width: 980px; height:auto; margin: 0px auto; }
		#header{width: auto; height:200px; background: red }
		#content{width: auto; height:500px; background: blue }
		#footer{width: auto; height:100px; background: green }

	</style>

</head>
<body>
	<div id="wrapper">
		<header id="header" class="">
			@section('sidebar')
				day la menu
			@show
		</header><!-- /header -->
		<div id="content">
			@yield('noidung')
		</div>
		<footer id='footer'> </footer>
	</div>
</body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('template',function(){
	return view('layout.master');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('thongtin',function(){
	return view('thontin');
});

Route::get('khoahoc',function(){
	return 'Hoc lap trinh laravel 5';
});

Route::get('khoahoc/{monhoc}/{thang}',function($monhoc,$thang){
	return "Khoc hoc lap trinh : $monhoc - thang $thang";
});

Route::get('thongtin/{hoten}/{sodienthoai}',function($hoten,$sodienthoai){
	return "Toi ten la: $hoten. So dien thoai: $sodienthoai";
})->where(['hoten'=>'[a-zA-Z]+','sodienthoai'=>'[0-9]{3,10}']);

Route::get('call-view',function(){
	$hoten = 'Hiep dep trai';
	$admin = 'view';
	return view('view',compact('hoten','admin'));
});

Route::get('test-controller','WelcomController@testAction');

Route::get('ho-chi-minh',['as'=>'hcm',function(){
	echo 'Hồ chí Minh dep lăm nguoi oi!';
}]);

Route::group(['prefix'=>'mon-an'],function(){
	Route::get('bun-bo',function(){
	echo 'Day la trang ban bun bo';
	});

	Route::get('bun-nam',function(){
		echo 'Day la trang ban bun nam';
	});

	Route::get('bun-mam',function(){
		echo 'Day la trang ban bun mam';
	});
});

Route::get('goi-view',function(){
	return view('layout.sub.view');
});

get('goi-layout',function(){
	return view('layout.sub.layout');
});

view()->share('title','Hoc lap trinh laravel 5');

view()->composer(['layout.sub.view','layout.sub.layout'], function($view) {
	return $view->with('thongtin','Thong tin ca nhan');
});

Route::get('check-view',function(){
	if (view()->exists('layout.sub.view'))
	{
		echo "true";
	}else{
		echo 'false';
	}
});

Route::get('goi-sub',function(){
	return view('views.layout');
});

Route::get('schema/create',function()
{
	Schema::create('duchiep', function($table) {
	    $table->increments('id');
	    $table->string('khoahoc');
	    $table->integer('gia');
	    $table->text('ghichu')->nullable();
	    $table->timestamps();
	
	});
});

Route::get('schema/rename',function(){
	Schema::rename('duchiep', 'dh');
});

Route::get('schema/drop',function(){
	Schema::drop('product');
});

Route::get('schema/drop-exist',function(){
	Schema::dropIfExists('dh');
});

$router->get('schema/change-col', function() {
	Schema::table('duchiep', function($table) {
		$table->string('khoahoc',50)->change();
	});
});

$router->get('schema/drop-col', function() {
	Schema::table('duchiep', function($table) {
		$table->dropColumn('khoahoc');
	});
});

$router->get('schema/drop-many-col', function() {
	Schema::table('duchiep', function($table) {
		$table->dropColumn(['khoahoc','gia']);
	});
});


$router->get('schema/create/category', function() {
	Schema::create('category', function($table) {
	    $table->increments('id');
	    $table->string('name');
	    $table->timestamps();
	});
});

$router->get('schema/create/product', function() {
	Schema::create('product', function($table) {
	    $table->increments('id');
		$table->string('name');
		$table->integer('pice');
		$table->integer('cad_id')->unsigned();
		$table->foreign('cad_id')->references('id')->on('category')->onDelete('cascade');
	});
});

$router->get('query/select-all', function() {
	$data = DB::table('product')->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/select-column', function() {
	$data = DB::table('product')->select('name')->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/orWhere', function() {
	$data = DB::table('product')->where('cad_id',1)->orWhere('pice',120000)->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/where', function() {
	$data = DB::table('product')->where('cad_id',1)->where('pice',120000)->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/order', function() {
	$data = DB::table('product')->select('name')->where('pice','>',120000)->orderBy('name',"ASC")->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

Route::get('query/limit',function(){
	$data = DB::table('product')->skip(2)->take(5)->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/between', function() {
	$data = DB::table('product')->whereBetween('id',[3,4])->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/notbetween', function() {
	$data = DB::table('product')->whereNotBetween('id',[3,4])->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/where-in', function() {
	$data = DB::table('product')->whereIn('id',[3,4,5,6])->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/where-notin', function() {
	$data = DB::table('product')->whereNotIn('id',[3,4,5,6])->get();
	echo '<pre>';		# code...
		print_r($data);
	echo '</pre>';		# code...
});

$router->get('query/count', function() {
	$data = DB::table('product')->count();
	print_r($data);
});

$router->get('query/max', function() {
	$data = DB::table('product')->max('pice');
	print_r($data);
});

$router->get('query/min', function() {
	$data = DB::table('product')->min('pice');
	print_r($data);
});

$router->get('query/avg', function() {
	$data = DB::table('product')->avg('pice');
	print_r($data);
});

$router->get('query/sum', function() {
	$data = DB::table('product')->sum('pice');
	print_r($data);
});

$router->get('schema/create/cate-new', function() {
	Schema::create('category_new', function($table) {
	    $table->increments('id');
	    $table->string('name');
	    $table->timestamps();
	});
});

$router->get('schema/create/product-new', function() {
	Schema::create('product_new', function($table) {
	    $table->increments('id');
		$table->string('title');
		$table->string('intro');
		$table->integer('cad_id')->unsigned();
		$table->timestamps();
	});
});

Route::get('query/join',function(){
	$data = DB::table('category_new')
			->select('name','title','intro')
			->join('product_new','product_new.cad_id','=','category_new.id')
			->get();
	echo '<pre>';
	print_r($data);
	echo '</pre>';
});

Route::get('query/leftjoin',function(){
	$data = DB::table('category_new')
			->select('name','title','intro')
			->leftJoin('product_new','product_new.cad_id','=','category_new.id')
			->get();
	echo '<pre>';
	print_r($data);
	echo '</pre>';
});

Route::get('query/rightjoin',function(){
	$data = DB::table('category_new')
			->select('name','title','intro')
			->rightJoin('product_new','product_new.cad_id','=','category_new.id')
			->get();
	echo '<pre>';
	print_r($data);
	echo '</pre>';
});

Route::get('query/insert',function(){
	DB::table('product')->insert([
			['name'=>'Quan di bien 1', 'pice'=>120000, 'cad_id'=> 1],
			['name'=>'Quan di bien 2', 'pice'=>130000, 'cad_id'=> 1],
			['name'=>'Quan di bien 3', 'pice'=>140000, 'cad_id'=> 1],
			['name'=>'Quan di bien 4', 'pice'=>150000, 'cad_id'=> 1]
		]);
	return 'insert thanh cong';
});

Route::get('query/insert-getid',function(){
	$id = DB::table('product')->insertGetId([
			'name' => 'insert get id',
			'pice' => 160000,
			'cad_id' => 2 
		]);
	echo $id;
});

$router->get('query/update', function() {
	DB::table('product')->where('id','<',20)->update(['name'=>'quan di ngu','pice' => 110000]);
});

$router->get('query/delete', function() {
	DB::table('product')->where('id','<',20)->delete();
});




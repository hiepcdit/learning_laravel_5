<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomController extends Controller
{
    public function testAction()
    {
    	return redirect()->route('hcm');
    }
}
